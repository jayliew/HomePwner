//
//  BNRItemStore.m
//  HomePwner
//
//  Created by Jay Liew on 7/12/15.
//  Copyright (c) 2015 Jay Liew. All rights reserved.
//

#import "BNRItemStore.h"
#import "BNRItem.h" // import this because we actually have to use it (unlike in our header)

@interface BNRItemStore ()

@property (nonatomic) NSMutableArray *privateItems;

@end

@implementation BNRItemStore

+ (instancetype)sharedStore // class method
{
    static BNRItemStore *sharedStore; // static var not destroyed when method
    // is done executing. like global var, not kept on stack.
    // Strong reference to BNRStoreItem
    
    if(!sharedStore){ // create if doesnt exist
        sharedStore = [[self alloc] initPrivate];
    }
    return sharedStore;
}

// if someone accidentally calls [[BNRItemStore alloc] init]
// let him know of problem
- (instancetype)init
{
    [NSException raise:@"Singleton" format:@"Use +[BNRItemStore sharedStore]"];
     return nil;
}

// this is the real (secret) initializer
- (instancetype)initPrivate
{
    self = [super init];
    
    if(self){
        _privateItems = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)moveItemAtIndex:(NSUInteger)fromIndex
                toIndex:(NSUInteger)toIndex
{
    if (fromIndex==toIndex) {
        return;
    }
    
    // Get pointed to object being moved that is to be re-inserted
    BNRItem *item = self.privateItems[fromIndex];
    
    // Remove item from array
    [self.privateItems removeObjectAtIndex:fromIndex];
    
    // Insert item in array at new location
    [self.privateItems insertObject:item atIndex:toIndex];
}

- (void)removeItem:(BNRItem *)item
{
    [self.privateItems removeObjectIdenticalTo:item];
}

- (NSArray *)allItems
{
    return [self.privateItems copy];
}

- (BNRItem *)createItem
{
    BNRItem *item = [BNRItem randomItem];
    [self.privateItems addObject:item];
    return item;
}

@end