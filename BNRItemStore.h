
//
//  BNRItemStore.h
//  HomePwner
//
//  Created by Jay Liew on 7/12/15.
//  Copyright (c) 2015 Jay Liew. All rights reserved.
//

#import <Foundation/Foundation.h>

/* This @class BNRItem tells compiler of existence of BNRItem class, but no need to know details except that
 it exists. No need to import its header; speeds up compilation. Allows use for createItem 
 wihout importing its header. */
@class BNRItem;

@interface BNRItemStore : NSObject

@property (nonatomic, readonly, copy) NSArray *allItems;

+ (instancetype)sharedStore; // class method
- (BNRItem *)createItem; // instance method
- (void)removeItem:(BNRItem *)item;
- (void)moveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;

@end